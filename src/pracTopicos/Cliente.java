
package chat;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JList;


/**
 *
 * @author RM
 */
public class Cliente {
JFrame ventana_cliente =null;
JButton botonEnviar = null;
JTextField mensajes = null;
JTextField nombre_usuario=null;
JTextArea areaChat = null ;
JPanel contenedorAreaChat = null;
JPanel contenedorBoton = null;
JPanel contenedor_nombreUsuario = null;
JPanel contenedor_conectados,contenedor_conectados1=null;
JScrollPane scroll = null;
JLabel nombre,conectados = null;
Socket clientSocket;
BufferedReader lector =null;
PrintWriter escritor = null;
private DataOutputStream salida;
JList listaConectados=null;

public Cliente(){
  crearInterfaz();  
}

public void crearInterfaz(){
    ventana_cliente = new JFrame("cliente");
    botonEnviar= new JButton("enviar");
    mensajes= new JTextField(20);
    nombre_usuario = new JTextField();
    areaChat = new JTextArea(10,12);
    areaChat.setEditable(false);
    scroll = new JScrollPane(areaChat);
    contenedorAreaChat = new JPanel();
    contenedorAreaChat.setLayout(new GridLayout(1,4));
    contenedorAreaChat.add(scroll);
    
    nombre=new JLabel("Nombre usuario");
    conectados=new JLabel("CONECTADOS");
    contenedor_nombreUsuario=new JPanel();
    contenedor_nombreUsuario.setLayout(new GridLayout(2,2));//filas derecha columnas izquierda 
    contenedor_nombreUsuario.add(nombre);
    contenedor_nombreUsuario.add(conectados,BorderLayout.EAST);
    contenedor_nombreUsuario.add(nombre_usuario);
    
    listaConectados=new JList();//se crea la lista para los conectados
    contenedor_conectados= new JPanel();//se crea el panel para la lista
    //contenedor_conectados1= new JPanel();
    contenedor_conectados.setLayout(new GridLayout(1,2));
    contenedor_conectados.add(listaConectados);

    contenedorAreaChat.add(contenedor_conectados,BorderLayout.EAST);// en el panel se agrega la lista
        
    contenedorBoton = new JPanel();
    contenedorBoton.setLayout(new GridLayout(1,2));
    contenedorBoton.add(mensajes);
    contenedorBoton.add(botonEnviar);
    ventana_cliente.setLayout(new BorderLayout());
    ventana_cliente.add(contenedorAreaChat,BorderLayout.CENTER);
    ventana_cliente.add(contenedorBoton,BorderLayout.SOUTH);
    
    ventana_cliente.add(contenedor_nombreUsuario,BorderLayout.NORTH);
    //ventana_cliente.add(listaConectados,BorderLayout.EAST);
    //ventana_cliente.add(contenedor_conectados,BorderLayout.EAST);
    
    ventana_cliente.setSize(400,320);
    ventana_cliente.setVisible(true);
    //ventana_cliente.setResizable(false);
    ventana_cliente.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    
    Thread principal = new Thread(new Runnable(){
        public void run(){
           
            try{ 
                clientSocket = new Socket("192.168.56.1",3333);
               
                    leer();
                    escribir();
                
            }catch(Exception e){
              Logger.getLogger(Cliente.class.getName()).log(Level.SEVERE, null, e);  
            }
        }
    });
    principal.start();
}

public void leer(){
    
   
   Thread leer_hilo = new Thread (new Runnable(){
       public void run(){
           //String mensaje_recibido=null;
         try{
        lector = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
        while(true){
          String mensaje_recibido = lector.readLine();
           areaChat.append("servidor dice: "+ mensaje_recibido+"\n");
        }
   
    }catch(Exception e){
        Logger.getLogger(Cliente.class.getName()).log(Level.SEVERE, null, e);
        
    }  
       }
   });
   leer_hilo.start();
    
}
public void escribir(){
    Thread escribir_hilo=new Thread(new Runnable(){
        public void run(){
            try{
        escritor = new PrintWriter(clientSocket.getOutputStream(),true);
        botonEnviar.addActionListener(new ActionListener(){
            public void actionPerformed(java.awt.event.ActionEvent e){
              /*  try{
                    salida= new DataOutputStream(clientSocket.getOutputStream());
                    
                }catch(Exception evt){
                    
                }*/
               String enviar_mensaje=mensajes.getText();
           escritor.println(enviar_mensaje);
           mensajes.setText("");
           areaChat.append("yo digo: "+enviar_mensaje+"\n");
            }
        });
        
    }catch (Exception e){
          Logger.getLogger(Cliente.class.getName()).log(Level.SEVERE, null, e);
    }
        }
    });
    escribir_hilo.start();
    
}


































    public static void main(String[] args) {
      new Cliente(); 
    }
    
}
